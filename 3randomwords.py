#!/usr/bin/python
"""3 Random Words Password Generator"""
# Author: Tim Cumming
# Created: 02/10/21
# Modified: 01/05/22

import argparse
import re
from pathlib import Path
from random import choice
import textwrap

wordlist_filenames = [
    'eff_large_wordlist.txt',
    'eff_short_wordlist_1.txt',
    'eff_short_wordlist_2.txt'
]


def load_dict(dict_file):
    wordlist = []
    if dict_file.exists():
        # open file in read mode
        f = open(dict_file, 'r')

        # display content of the file
        wordlist = f.read().splitlines()

        # close the file
        f.close()
    return wordlist


def select_words(word_list, count):
    selections = []

    # Make choices from the word list
    # Check that it's not a duplicate
    while len(selections) != count:
        word = choice(word_list)
        if word not in selections:
            # title() is more to show the individual words
            # TODO: Perhaps an option to be added for the user
            selections.append(word.title())

    return selections


class ThreeRandomWords:
    def __init__(self, args):
        self.args = args
        print("[**] Minimum word length: %s" % self.args.min)
        print("[**] Maximum word length: %s" % self.args.max)
        print("[**] Number of Passwords to generate: %s" % self.args.count)

    def run(self):
        data = []
        for t in wordlist_filenames:
            words_from_file = load_dict(Path('data', t))
            for w in words_from_file:
                if w not in data:
                    data.append(w)

        # Only going to use single word items of min & max length
        min_length = 3
        max_length = 32

        pattern = r'^[a-zA-Z]{' + str(self.args.min) + ',' + str(self.args.max) + '}$'

        filtered_list = list(filter(lambda word: re.match(pattern, word), data))

        pool_size = len(filtered_list)

        if pool_size < 1:
            print("[!] No Selections Available")
        else:
            print("[**] %i Random words from: %i\n" % (self.args.words, pool_size))

            for x in range(self.args.count):
                random_words = select_words(filtered_list, self.args.words)

                for w in random_words:
                    print("%s" % w, end='')

                print("\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='3 Random Words Password Generator',
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent('''Example:
          3randomwords.py -m 3 -x 32 -w 3
          '''))
    parser.add_argument('-m', '--min', type=int, default=3, help='Minimum word length')
    parser.add_argument('-x', '--max', type=int, default=32, help='Maximum word length')
    parser.add_argument('-w', '--words', type=int, default=3, help='Number of words to select')
    parser.add_argument('-c', '--count', type=int, default=1, help='Number of Passwords to output')

    args = parser.parse_args()

    trw = ThreeRandomWords(args)
    trw.run()
