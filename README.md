3RandomWords
---

A password suggesting app using the "Three Random Words" solution.

At the moment; just a small script to pick three random words from a selection stored in a data file.

As an example I have used the EFF dice word lists as a source. (https://www.eff.org/dice)

TODO:
- Selections from a theme?
- Choice of prefix/suffix/none
- Choice of non alpha word separators

Reference:
NCSC Post - https://www.ncsc.gov.uk/blog-post/the-logic-behind-three-random-words